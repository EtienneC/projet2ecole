package spring.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//spring boot spécifique args parametre du main et la class c'est celle en cours
		SpringApplication.run(Application.class, args);
		
	}

}
