package spring.mvc;

import org.springframework.web.client.RestTemplate;

public class CookBookClient {
	public static void main(String[] args) {
		try {
			
			//on créée un objet de type RestTemplate
			RestTemplate rt = new RestTemplate();
			Boolean complete = rt.getForObject("http://localhost:8081/complete", Boolean.class);
			System.out.println("complet? "+complete);
			
			System.out.println("recette : "+rt.getForObject("http://localhost:8081/recipe/1", Recipe.class).getName());
			
			System.out.println("WIKIPEDIA : "+rt.getForObject("https://fr.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=Onde_gravitationnel", Object.class));
			
			
			rt.delete("http://localhost:8081/recipes/2");
			
			rt.postForLocation("http://localhost:8081/recipes", new Recipe("coco",50,"les bons cocos paimpolais"), Recipe.class);
			
			
			
			
			
			
		}catch(Exception ex) {
			//on affiche les exceptions
			ex.printStackTrace();
		}
	}
}
