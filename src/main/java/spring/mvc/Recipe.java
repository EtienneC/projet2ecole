package spring.mvc;

public class Recipe {

	private String name;
	private int duration;
	private String text;
	
	//Constructeur
	public Recipe() {
		super();
	}
	public Recipe(String name, int duration, String text) {
		super();
		this.name = name;
		this.duration = duration;
		this.text = text;
	}
	
	//Methode to string :
	@Override
	public String toString() {
		return "Name : "+name+". Duration : "+duration+". Text : "+text;
	}
	
	//Getter and Setter
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
	
}
