package spring.mvc;

public class Book {
	
	//attributs
	private int id;
	private String title;
	private String editor;
	private int year;
	private String authorName;
	private String authorFirstName;
	
	//constructor
	public Book() {
		super();
	}
	
	//constructor with arg
	public Book(String title, String editor, int year, String authorName, String authorFirstName) {
		super();
		this.title = title;
		this.editor = editor;
		this.year = year;
		this.authorName = authorName;
		this.authorFirstName = authorFirstName;
	}
	
	//getters and setters
//	public int getId() {
//		return id;
//	}
//	public void setId(int id) {
//		this.id = id;
//	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

}
