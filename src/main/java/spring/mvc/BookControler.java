package spring.mvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookControler {
	
	//telle url arrive sur telle méthode il faut donc faire un request mapping
	@RequestMapping(value="/complete", method=RequestMethod.GET, /*consumes="application/json",*/ produces="application/json")
	public boolean isComplete() {
		return false;
	}
	

	
	@GetMapping(value="/books", produces="application/json")
	List<Book> getBooks() {
		List<Book> books = new ArrayList<Book>();
		
		
		
		Book book1 = new Book("titre","editor",1200,"roger","robert");
		
	books.add(book1);
		return books;
	}
	
	
	
	@GetMapping(produces="application/json", value="/recipe/{n}")
	public Book getRecipe(@PathVariable("n")int n) {
		return getBooks().get(n);
	}
	
	@DeleteMapping("/recipes/{n}")
	public void deleteRecipe(@PathVariable("n") int n) {
		//deleteRecipe(n);
		System.out.println("Recette supprimée : "+n);
	}
	
	//on ajoute juste au conteneur et sera mis naturellement à la fin
	@PostMapping("/recipes")
	public void addRecipe(@RequestBody Recipe r) {
		System.out.println("Recette ajoutée : "+r.getName());
	}
	
}
